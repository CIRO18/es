import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Main {

    public static void main(String[] args) {

        crea_arraylist();
        crea_arrayClassico();

    }

    private static void crea_arraylist() {

        ArrayList<String> array = new ArrayList<String>();

        array.add("Cassaforte con Combinazione");
        array.add("Frankenstein");
        array.add("Semaforo verticale");
        array.add("Semaforo orizzonatale");
        array.add("Cassaforte senza Combinazione");

        array.remove(3);
        Collections.reverse(array);

        for (String s : array) {
            System.out.println(s);
        }

    }

    private static void crea_arrayClassico() {

        String[] arrayClassico = new String[5];

        arrayClassico[0] = "Cassaforte con Combinazione";
        arrayClassico[1] = "Frankenstein";
        arrayClassico[2] = "Semaforo verticale";
        arrayClassico[3] = "Semaforo orizzonatale";
        arrayClassico[4] = "Cassaforte senza Combinazione";

        arrayClassico[3] = "";
        Collections.reverse(Arrays.asList(arrayClassico));

        System.out.println("\n-----------------------------------\n");

        for (int i = 0; i < arrayClassico.length; i++) System.out.println(arrayClassico[i]);

    }

}
